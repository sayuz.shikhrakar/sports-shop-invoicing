var count = 1;
var data = [];
var itemTotalPrice;
var itemPrice;
var itemSelected = [];
var quantity = [];
var quantityPrice = [];
var grandTotalPrice;
var prodcutLineTemplate = `   
    <div class="product-line">
        <select id="select-item">  
            <option value="Table Tennis Racket" data-price="800">Table Tennis Racket</option>
            <option value="Badminton Racket" data-price="1500">Badminton Racket</option>
            <option value="Football " data-price="1200">Football</option>
            <option value="Basketball " data-price="1100">Basketball </option>
            <option value=" Football gloves " data-price="1900"> Football gloves </option>
            <option value=" Football Shoes  " data-price="2100"> Football Shoes  </option>
            <option value="Chess" data-price="350">Chess  </option>
            <option value="Carrom Board " data-price="1400">Carrom Board </option>
        </select>
        Quantity
        <input type="number" id="item-quantity" name="Quantity"min="1" max="50">
        <button  class="add-item-total" id="add-item-total">Item Total</button>
        <button  class="btn-remove-product-line" id="btn-remove-product-line">Remove</button>
        <p id="item-price"></p>
    </div>`;

var billTemplate = '';
//generates unique id for every product line
function unqiueIdGenerator() {
    $('.product-line').each(function (id) {
        var uniqueId = $(this).attr('id', 'product-' + id);
        return uniqueId;
    });
}
//to add new product line
function addNewProductLine() {
    $('#test').append(prodcutLineTemplate);
    
}
//remove the product line
function removeProductLine() {

    $('#btn-remove-product-line').closest('.product-line').remove();
}
//totaling of an individual item
function singleItemTotal(addBtn) {

    var parentId = addBtn.parent().attr('id');

    $.each($("#" + parentId + " #select-item option:selected"), function () {
         itemPrice = ($(this).data('price'));

        var qty = $("#" + parentId + " #item-quantity").val();

        var singleItemTotal = itemPrice * qty;

        $("#" + parentId + " .item-price").text(singleItemTotal);
        data.push($(this).data('price'));
        itemSelected.push($("#" + parentId + " #select-item").val());
        quantity.push($("#" + parentId + " #item-quantity").val());
        quantityPrice.push(singleItemTotal);
    });
    console.log(quantity);
    console.log(quantityPrice);


}
//  itemTotalPrice;
function itemTotal() {
    console.log(data);
    var sum = 0;


    for (var i = 0; i < quantityPrice.length; i++) {
        sum += quantityPrice[i];
    }
    itemTotalPrice = sum;
    console.log($('#billTotal'));
    console.log($('#billTotal').append(`<p>${sum}</p>`));
    return sum;

}
//calculate the discount from  a grand total
function calculateDiscount() {
    sum = 0;
    for (var i = 0; i < quantityPrice.length; i++) {
        sum += quantityPrice[i];
    }
    grandTotalPrice = sum;

    if (itemTotal < 3000) {
        $('#item-total').append(grandTotalPrice);
    } else if (grandTotalPrice >= 3000 && itemTotalPrice <= 6000) {
        var discountPrecent = (20 / 100) * grandTotalPrice;
        var discount = grandTotalPrice - discountPrecent;
        $('#item-total').append(discount);
    }
    else {
        var discountPrecent = (30 / 100) * grandTotalPrice;
        var discount = grandTotalPrice - discountPrecent;
        $('#item-total').append(discount);
    }
    console.log(discount);
    return discountPrecent;
}

function showItemPrice(){
    $.each($("#select-item option:selected"), function () {
        itemPrice = ($(this).data('price'));
        console.log(itemPrice);
        return itemPrice;

    })}


//grand total

$(document).ready(function () {

    $('#btn-add-product-line').on('click', function () {
        addNewProductLine();
        unqiueIdGenerator();
      
    });

    $(document).on('click', '#btn-remove-product-line', function () {
        removeProductLine();

    });

    $(document).on('click', '#add-item-total', function () {
        singleItemTotal($(this));
       

    });

    $('#item-price').change(function(){
        showItemPrice($(this));
        Console.log(showItemPrice());  
    })
    //generate an array to display JSON
    $(document).on('click', '#genBill', function () {
        $('#product-div').remove();
        // $('#product-div').css('visibility','hidden')
        $('#billTable').css('visibility', 'visible')
        var i = itemTotal();
        $('#generate-invoice-button').css('visibility', 'hidden');
        $('#genBill').css('visibility', 'hidden');
        $('#billData').append(billTemplate);
        for (let i = 0; i < itemSelected.length; i++) {
            var bodyData =
                '<tr>' +
                '<td>' + itemSelected[i] + '</td>' +
                '<td>' + data[i] + '</td>' +
                '<td>' + quantity[i] + '</td>' +
                '<td>' + quantityPrice[i] + '</td>' +
                '</tr>';
            $('#dataDisplay').append(bodyData);
            var jasonArray = [];
            var dataJson = { 'item': itemSelected[i], 'quantity': quantity[i], 'unit-price': data[i], 'total_price': quantityPrice[i] };

            jasonArray.push(dataJson);
            jasonArray.forEach(itemarr => {
                console.log(itemarr);
            })
        }
        var dis = calculateDiscount();
        $('#billDiscount').text(dis);
        $('#billGrandTotal').text(i - dis);
    });


    $('#reload-button').click(function () {
        document.location.reload();
    })



});
